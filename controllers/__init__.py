import uuid
import settings
import logging

from functools import wraps
from flask import (request, session, abort,
                   make_response, render_template, redirect, url_for)

from models import User

from libs.pagination import Paginator


def authenticated(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not 'fb_id' in session:
            logging.info('not session')
            return redirect(url_for('home'))
        return f(*args, **kwargs)
    return decorated


def csrf_protect():
    if request.method == "POST":
        token = session.pop('csrf_token', None)
        if not token or token != request.form.get('csrf_token'):
            abort(403)


def generate_csrf_token():
    session['csrf_token'] = '%s%s' % (
        str(uuid.uuid4()),
        settings.COOKIE_SECRET
    )
    return session['csrf_token']


class RequestHandler(object):

    def render_template(self, name, **kwargs):
        r = make_response(render_template(name, **kwargs))
        r.headers["P3P"] = settings.P3P_COMPACT
        return r

    @property
    def current_user(self):
        if not hasattr(self, "_current_user"):
            self._current_user = self.get_current_user()
        return self._current_user

    def get_current_user(self):
        if not 'fb_id' in session:
            return None
        fb_id = session.get('fb_id')
        user = User.query.filter_by(fb_id=fb_id).first()
        return user


class ListMixin(object):

    @property
    def current_page(self):
        current_page = request.form.get('page', '1')
        return int(current_page) if current_page.isdigit() else 1

    def get_pagination(self, query, count, per_page=10):
        try:
            per_page = int(per_page)
        except ValueError:
            per_page = 10
        page = self.current_page
        paginator = Paginator(page=self.current_page, total_items=count,
                              per_page=per_page)
        per_page = paginator.per_page
        return {
            'items': query[(page - 1) * per_page: page * per_page],
            'pages': paginator.pages,
            'total_items': count,
            'current_page': page,
            'total_pages': paginator.total_pages,
        }
