import logging
import settings

from flask.views import MethodView
from models import User
from controllers import RequestHandler, authenticated

from flask import (
    url_for, request, redirect,
    session, abort
)

from libs import facebook
from datetime import datetime
from database import db_session


def open_graph(self):
    data = {}
    data['app_id'] = settings.FACEBOOK_API_KEY
    data['title'] = 'WineFest'
    data['image'] = '%s%s' % (
        settings.DOMAIN_URL,
        url_for('static', 'img/90x90.jpg')
    )
    data['url'] = settings.FACEBOOK_CANVAS_PAGE
    data['type'] = 'papa-extraordinario:papa_extraordinario'
    return data



class Home(MethodView, RequestHandler):

    def get(self):
        return self.post()

    def post(self):
        if settings.DISPLAY_WINNERS:
            if datetime.now().strftime(
                    "%Y-%m-%d %H:%M:%S") >= settings.WINNERS_START_TIME:
                return redirect(url_for('winners'))

        signed_request = request.form.get('signed_request')

        if not signed_request:
            abort(403)

        user = facebook.parse_signed_request(
            signed_request,
            settings.FACEBOOK_API_SECRET
        )

        if not user:
            abort(403)

        if not 'user_id' in user:
            return facebook.auth(
                settings.FACEBOOK_APPLICATION_INITIAL_PERMISSIONS,
                settings.FACEBOOK_CANVAS_PAGE
            )
        else:
            fb_id = user.get('user_id')
            expires = user.get('expires')
            oauth_token = user.get('oauth_token')

            fields = (
                'id',
                'first_name',
                'last_name',
                'email',
                'gender',
            )

            profile = facebook.info(
                oauth_token,
                ','.join(fields)
            )

            user = User.query.filter_by(fb_id=fb_id).first()

            if not user:
                logging.info('new user')
                user = User()
                user.fb_id = fb_id
                user.oauth_token = oauth_token
                user.first_name = profile.get('first_name')
                user.last_name = profile.get('last_name')
                user.fb_first_name = profile.get('first_name')
                user.fb_last_name = profile.get('last_name')
                user.fb_gender = profile.get('gender')
                user.fb_email = ''
                user.fb_oauth_token = oauth_token
                user.fb_expires = expires

                if profile.get('email'):
                    if not "@proxymail.facebook" in profile.get('email'):
                        user.fb_email = profile.get('email')
                db_session.add(user)

                try:
                    db_session.commit()
                except Exception as exc:
                    logging.error(exc)
                    db_session.rollback()
                    db_session.remove()
                else:
                    session['fb_id'] = fb_id
                    logging.info('save user')
                    db_session.remove()
            else:
                session['fb_id'] = fb_id
                logging.info('exists user')

            send_data = dict(
                title='Home',
                display_google_analitycs=settings.DISPLAY_GOOGLE_ANALITYCS,
                facebook_send_google=settings.FACEBOOK_SEND_OG,
            )

            if settings.FACEBOOK_SEND_OG:
                send_data.update({'og': open_graph()})
            return self.render_template('home.html', **send_data)


class AddComment(MethodView, RequestHandler):

    @authenticated
    def post(self):
        comment = request.form.get('comment')

        user = self.current_user
        user.comment = comment
        db_session.add(user)
        try:
            db_session.commit()
        except Exception as exc:
            logging.error(exc)
            db_session.rollback()
            db_session.remove()
        else:
            db_session.remove()
        return redirect('form')


class Terms(MethodView, RequestHandler):

    def get(self):
        return self.render_template(
            'terms.html',
            title='Terminos',
        )


class Thanks(MethodView, RequestHandler):

    @authenticated
    def get(self):
        return self.render_template(
            'thanks.html',
            title='Gracias',
        )


class Winners(MethodView, RequestHandler):

    def get(self):
        return self.render_template(
            'winners.html',
            title='Ganadores',
        )
