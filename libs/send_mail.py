import smtplib
import html2text

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE


EMAIL_CONFIG = {
    'smtp_server': 'smtp.gmail.com',
    'smtp_port': 587,
    'smtp_password': 'm4cf1_123',
    'smtp_username': 'm4cf1.server',
    'from': 'm4cf1_server@gmail.com',
}


class Mail(object):

    def __init__(self, subject='', sender='', to=None, cc=None, bcc=None):
        """
        @to list
        @cc list
        @bcc list
        """
        def to_list(val):
            if isinstance(val, list):
                return val
            else:
                return list(val) if val else []

        self._subject = subject
        self._sender = sender
        self._to = to_list(to)
        self._cc = to_list(cc)
        self._bcc = to_list(bcc)
        self._body = []

    @property
    def subject(self):
        return self._subject

    @subject.setter
    def subject(self, value=""):
        self._subject = value

    @property
    def sender(self):
        return self._sender

    @sender.setter
    def sender(self, value=""):
        self._sender = value

    @property
    def to(self):
        return self._to

    @to.setter
    def to(self, value=[]):
        if isinstance(value, str):
            value = [value]
        elif isinstance(value, tuple):
            value = list(value)

        if isinstance(value, list):
            self._to = value

    @property
    def cc(self):
        return self._cc

    @cc.setter
    def cc(self, value=[]):
        if isinstance(value, str):
            value = [value]
        elif isinstance(value, tuple):
            value = list(value)

        if isinstance(value, list):
            self._cc = value

    @property
    def bcc(self):
        return self._bcc

    @bcc.setter
    def bcc(self, value=[]):
        if isinstance(value, str):
            value = [value]
        elif isinstance(value, tuple):
            value = list(value)

        if isinstance(value, list):
            self._bcc = value

    def add_body(self, content, type="plain"):
        self._body.append(MIMEText(content.encode("utf-8"), type, "utf-8"))

    def as_string(self):
        _mail = MIMEMultipart("alternative")
        _mail["Subject"] = self._subject
        _mail["From"] = self._sender
        _mail["To"] = COMMASPACE.join(self._to)
        if len(self._cc) > 0:
            _mail["CC"] = COMMASPACE.join(self._cc)
        for b in self._body:
            _mail.attach(b)
        return _mail.as_string()


def detect(request):
    browser = request.user_agent.browser
    version = request.user_agent.version and \
        int(request.user_agent.version.split('.')[0])
    platform = request.user_agent.platform
    uas = request.user_agent.string

    return '%s | %s | %s |' % (
        browser,
        version,
        platform, uas
    )


def send_email(mail):
    try:
        conn = smtplib.SMTP()
        conn.connect(
            EMAIL_CONFIG['smtp_server'],
            EMAIL_CONFIG['smtp_port']
        )
        conn.ehlo()
        conn.starttls()
        conn.ehlo()

        conn.login(
            EMAIL_CONFIG['smtp_username'],
            EMAIL_CONFIG['smtp_password']
        )

        conn.sendmail(
            EMAIL_CONFIG['from'],
            mail._to + mail._cc + mail._bcc,
            mail.as_string()
        )
    except smtplib.SMTPException as exc:
        print exc


def send_mail_to(request, to):

    mail = Mail(
        subject=u'Error Dolce - diapapa',
        sender='m4cf1_server@gmail.com',
        to=to
    )

    content = request
    content_plain = html2text.html2text(content)
    mail.add_body(content_plain, 'plain')
    mail.add_body(content, 'html')
    send_email(mail)


if __name__ == '__main__':

    send_mail_to(
        'ola como estas',
        ['macfri10@gmail.com', 'desarrollo@no2.pe']
    )
