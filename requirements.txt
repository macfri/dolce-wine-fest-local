Flask==0.9
Flask-Admin==1.0.6
Flask-Auth==0.85
Flask-Login==0.1.3
Flask-WTF==0.8.3
Jinja2==2.7
MarkupSafe==0.18
MySQL-python==1.2.4
SQLAlchemy==0.8.1
WTForms==1.0.4
Werkzeug==0.8.3
argparse==1.2.1
simplejson==3.3.0
wsgiref==0.1.2
