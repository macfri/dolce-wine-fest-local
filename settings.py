DEBUG = False
DATABASE_DEBUG = DEBUG
DATABASE_DSN = ''

XSRF_COOKIES = True
SECRET_KEY = ''
COOKIE_SECRET = ""

HOST = '127.0.0.1'
PORT = 5000
DOMAIN_URL = ''

FACEBOOK_API_KEY = ''
FACEBOOK_API_SECRET = ''
FACEBOOK_DEBUG_TOKEN = ''
FACEBOOK_OAUTH = 'https://www.facebook.com/dialog/oauth/'
FACEBOOK_GRAPH = 'https://graph.facebook.com/'
FACEBOOK_ACCESS_TOKEN = 'https://graph.facebook.com/oauth/access_token'
FACEBOOK_CANVAS_PAGE = ''
FACEBOOK_SEND_OG = False

STATIC_URL = '/static/'
HOME_URL = ''

P3P_COMPACT = 'policyref="%s%sP3P/p3p.xml", CP="NON DSP COR CURa TIA"' % (
    DOMAIN_URL,
    STATIC_URL
)

WINNERS_START_TIME = '2013-06-16 23:59:59'
DISPLAY_WINNERS = False
DISPLAY_GOOGLE_ANALITYCS = False

FACEBOOK_APPLICATION_INITIAL_PERMISSIONS= (
    'email',
    'friends_birthday',
    'read_stream',
    'user_birthday'
)

EMAIL_CONFIG = {
    'smtp_server': 'smtp.gmail.com',
    'smtp_port': 587,
    'smtp_password': '',
    'smtp_username': '',
    'from': '',
}

try:
    from local_settings import *
except ImportError:
    pass
